﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Oefening_28._7
{
    class DigitaleKluis
    {
        private int _code;
        private bool _canShowCode;
        private int _codeLevel;
        private int _aantalPogingen;
        System.Media.SoundPlayer alarm = new System.Media.SoundPlayer(@"D:\C# repos\Oefening 28.7\Sounds\alarm.wav");

        public DigitaleKluis(int code)
        {
            AantalPogingen = 0;
            Code = code;
        }

        public int Code 
        { 
            get 
            {
                int temp = -666;
                if (CanShowCode)
                {
                    temp = _code;
                }
                return temp;
            } 
            set {  _code = value; } 
        }
        public bool CanShowCode
        {
            get { return _canShowCode; }
            set { _canShowCode = value; }
        }
        public int CodeLevel
        {
            get 
            {
                _codeLevel = Code / 1000;
                return _codeLevel; 
            }
        }
        private int AantalPogingen
        {
            get { return _aantalPogingen; }
            set { _aantalPogingen = value; }
        }
        public bool TryCode(int ingegevenCode)
        {
            bool correct = false;
            AantalPogingen++;
            if (ingegevenCode == _code)
            {
                correct = true;
            }
            else if (ingegevenCode == -666)
            {
                alarm.Play();
                MessageBox.Show("Cheater Cheater Cheater", "Cheater", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return correct;
        }
        public static string CrackCode(DigitaleKluis kluis)
        {
            int CrackingCode = 0;

            while (kluis.TryCode(CrackingCode) == false)
            {
                CrackingCode++;
            }

            return "De code werd gekraakt. Aantal pogingen: " + kluis.AantalPogingen + " Code: " + kluis.Code;
        }
        public override string ToString()
        {
            return AantalPogingen.ToString();
        }
    }
}
