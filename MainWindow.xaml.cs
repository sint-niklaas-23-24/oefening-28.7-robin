﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening_28._7
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>


    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        DigitaleKluis kluis = new DigitaleKluis(8426);
        System.Media.SoundPlayer nice = new System.Media.SoundPlayer(@"D:\C# repos\Oefening 28.7\Sounds\noice.wav");
        System.Media.SoundPlayer wrong = new System.Media.SoundPlayer(@"D:\C# repos\Oefening 28.7\Sounds\wrong.wav");

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            imgKluis.Source = new BitmapImage(new Uri("Images/kluis_dicht.jpg", UriKind.Relative));
        }

        private void btnInvoeren_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (kluis.TryCode(Convert.ToInt32(txtCode.Password)))
                {
                    nice.Play();
                    kluis.CanShowCode = true;
                    lblInfo.Content = "De code is correct. Code: " + kluis.Code + " Aantal pogingen: " + kluis.ToString();
                    imgKluis.Source = new BitmapImage(new Uri("Images/kluis_open.jpg", UriKind.Relative));
                    btnInvoeren.IsEnabled = false;
                    btnKraakCode.IsEnabled = false;
                }
                else
                {
                    wrong.Play();
                    lblInfo.Content = "Dit is niet de juiste code. Code: " + kluis.Code;
                }
            }
            catch
            {
                MessageBox.Show("Geef een getal in.", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnKraakCode_Click(object sender, RoutedEventArgs e)
        {
            nice.Play();
            kluis.CanShowCode = true;
            lblInfo.Content = DigitaleKluis.CrackCode(kluis);
            imgKluis.Source = new BitmapImage(new Uri("Images/kluis_open.jpg", UriKind.Relative));
            btnInvoeren.IsEnabled = false;
            btnKraakCode.IsEnabled = false;
        }
    }
}
